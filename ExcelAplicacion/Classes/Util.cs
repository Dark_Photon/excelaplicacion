﻿using System.Diagnostics;
using System.IO;

namespace ExcelAplicacion.Classes
{
    public class Util
    {
        public void CreateDirectory(string name)
        {
            Debug.Assert(!ExistsDirectory(name));
            Directory.CreateDirectory(name);
        }

        public bool ExistsDirectory(string name)
        {
            return Directory.Exists(name);
        }      
    }
}
