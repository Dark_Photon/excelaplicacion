﻿using ClosedXML.Excel;
using ExcelAplicacion.Configuration;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace ExcelAplicacion.Classes
{
    public class Excel
    {
        private string name;
        private string extension;
        private string directory;
        private readonly string pathFull;
        private List<Row> listRows;
        private List<Table> listTables;
        private int numMaxColumns;
        private Config config;

        public List<Row> ListRows { get => listRows; }

        public Excel(Config config) : this(config.NameFile, config.Directory, config) { }

        public Excel(string nombre, string directorioFicheros, Config config)
        {
            this.setExcel(nombre, directorioFicheros, config);
            this.pathFull = this.directory + "//" + this.name + this.extension;
        }

        private void setExcel(string name, string directory, Config config)
        {
            this.name = name.Split(".")[0];
            this.extension = "." + name.Split(".")[1];
            this.directory = directory;
            this.config = config;
            this.listRows = new List<Row>();
            this.numMaxColumns = this.GetNumMaxColumns(config.CorrectHeaders);
            this.listTables = new List<Table>();
        }
        
        private Row getRowExcel(IExcelDataReader reader, int numColumnas)
        {
            Row row = new Row();
            for (int i = 0; i < numColumnas; i++)
            {
                row.AddToRow(reader.GetValue(i) + "");
            }
            return row;
        }

        public void Read()
        {
            //Codigo de codificación necesario para leer el fichero
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            FileStream stream = File.Open(pathFull, FileMode.Open, FileAccess.Read);
            IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);

            do
            {
                while (reader.Read())
                {
                    Row row = getRowExcel(reader, this.numMaxColumns);
                    if (!row.IsEmpty())
                    {
                        ListRows.Add(row);
                    }
                }
            } while (reader.NextResult());

            stream.Close();
            reader.Close();
        }

        public void ProcessData()
        {
            List<Row> data = this.listRows;                     
            List<Table> listTablesTest = filter(data, config.CorrectHeaders, config.IncorrectHeaders);                       

            SetListTable(listTablesTest);
        }

        private void SetListTable(List<Table> listTables)
        {
            this.listTables = listTables;
        }
        public List<Table> GetTables()
        {
            return listTables;
        }

        public void CopyProcessInfoForEachTable()
        {
            foreach (Table item in listTables)
            {
                item.CopyProcessInfo();
            }
        }

        public void CreateExcel()
        {
            CreateExcel(config.NameFileOut);
        }

        public void CreateExcel(string name)
        {
            CreateExcel(listTables, name);
        }

        public void CreateExcel(Table table, string name)
        {
            CreateExcel(new List<Table>() { table }, name);
        }
        public void CreateExcel(List<Table> listTable, string name)
        {
            int sheet = 1;
            XLWorkbook wb = new XLWorkbook();
            foreach (Table item in listTable)
            {                
                wb.Worksheets.Add(item.ConvertTableToDataTable(), "Sheet" + sheet);
                sheet++;
            }
            if (!name.Contains(".xlsx"))
            {
                wb.SaveAs(config.DirectoryOut + "//" + name + ".xlsx");
            }
            else
            {
                wb.SaveAs(config.DirectoryOut + "//" + name);
            }
        }
       
        private List<Table> AddListHeaderToTable(List<Row> listHeader)
        {
            List<Table> listT = new List<Table>();

            for (int i = 0; i < listHeader.Count; i++)
            {
                Row header = new Row(listHeader.ElementAt(i));
                listT.Add(new Table(header));
            }
            return listT;
        }

        public List<Table> filter(List<Row> datos, List<Row> listHeadersCorrect, List<Row> listHeadersIncorrect)
        {
            List<Table> newListTable = AddListHeaderToTable(listHeadersCorrect);
            bool CCorrecta = false;
            int indexTabla = -1;

            foreach (Row item in datos)
            {                
                Row rowWithoutEmptyCells = new Row(item.GetListData().Where(x => !string.IsNullOrEmpty(x)).ToList<string>());

                int indexCorrect = rowWithoutEmptyCells.GetIndexList(listHeadersCorrect);
                if (indexCorrect != -1)
                {
                    CCorrecta = true;
                    indexTabla = indexCorrect;
                }

                int indexIncorrect = rowWithoutEmptyCells.GetIndexList(listHeadersIncorrect);
                if (indexIncorrect != -1)
                {
                    CCorrecta = false;
                }

                bool filaIgualTest = rowWithoutEmptyCells.GetIndexList(listHeadersCorrect) != -1;

                if (CCorrecta && !filaIgualTest)
                {
                    newListTable.ElementAt(indexTabla).AddRow(item);
                }
            }

            return newListTable;
        }
        
        private int GetNumMaxColumns(List<Row> listRow)
        {
            return Row.GetMaxLength(listRow);
        }

    }

}
