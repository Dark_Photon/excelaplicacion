﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ExcelAplicacion.Classes
{
    public class Table
    {
        private Row header;
        private List<Row> listRows;

        public Table(Row header) : this(header, new List<Row>()) { }
        public Table(Row header, List<Row> listRows)
        {
            this.setTable(header, listRows);
        }
        private void setTable(Row header, List<Row> listRows)
        {
            this.header = new Row(header);
            this.listRows = Row.CloneList(listRows);
        }

        public void AddRow(Row row)
        {
            listRows.Add(row);
        }

        public List<Row> GetListRows()
        {
            return listRows;
        }

        public Row GetHeader()
        {
            return this.header;
        }

        public bool IsEqualHeader(Row header)
        {
            return this.header.IsEqual(header);
        }

        public bool IsEqualHeader(List<string> list)
        {
            return this.header.IsEqual(list);
        }

        public bool IsEqual(Table table)
        {
            bool headerBool = this.IsEqualHeader(table.header);

            for (int i = 0; i < this.listRows.Count; i++)
            {
                if (!this.listRows[i].IsEqual(table.listRows[i]))
                {
                    return false;
                }
            }

            return headerBool;
        }

        public void CopyProcessInfo()
        {
            Row auxInit = listRows.First().Clone();


            foreach (Row item in listRows)
            {
                for (int i = 0; i < item.GetListData().Count; i++)
                {
                    string cellAux = item.GetListData().ElementAt(i);

                    if (string.IsNullOrEmpty(cellAux))
                    {
                        item.GetListData()[i] = auxInit.GetListData().ElementAt(i);
                    }
                    else
                    {
                        auxInit.GetListData()[i] = cellAux;
                    }
                }
            }
        }

        public DataTable ConvertTableToDataTable()
        {
            DataTable dt = new DataTable();

            foreach (string item in header.GetListData())
            {
                dt.Columns.Add(item);
            }
            foreach (Row item in listRows)
            {
                DataRow dtRow = dt.NewRow();

                for (int i = 0; i < header.GetListData().Count; i++)
                {
                    string columna = header.GetListData().ElementAt(i);
                    dtRow[columna] = item.GetListData().ElementAt(i);
                }
                dt.Rows.Add(dtRow);
            }
            return dt;
        }

    }
}
