﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ExcelAplicacion.Classes
{
    public class Row
    {
        [JsonProperty("Header")]
        private List<string> listData;

        public Row() : this(new List<string>()) { }

        public Row(Row row) : this(row.listData) { }

        public Row(List<string> listData)
        {
            this.setRow(listData);
        }

        private void setRow(List<string> listData)
        {
            this.listData = (new List<string>(listData));
        }

        public void AddToRow(string data)
        {
            this.listData.Add(data);
        }

        public List<string> GetListData()
        {
            return listData;
        }
        public int Count()
        {
            return listData.Count;
        }
        public bool IsEqual(List<string> list)
        {
            return this.IsEqual(new Row(list));
        }
        public bool IsEqual(Row row)
        {            
            for (int i = 0; i < this.listData.Count; i++)
            {
                if (!this.listData[i].Equals(row.listData[i]))
                {
                    return false;
                }
            }
            return true;            
        }
        public bool IsEmpty()
        {
            foreach (string item in listData)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    return false;
                }
            }
            return true;
        }
        public int GetIndexList(List<Row> list)
        {
            int counter = 0;
            int index = -1;

            foreach (Row item in list)
            {
                if (item.IsEqual(this))
                {
                    index = counter;
                }
                counter++;
            }
            return index;

        }
        public static int GetMaxLength(List<Row> listRow)
        {
            int counter = 0;
            foreach (Row item in listRow)
            {
                if (item.Count() > 0)
                {
                    counter = item.Count();
                }
            }
            return counter;
        }

        public Row Clone()
        {
            return new Row(this);
        }

        public static List<Row> CloneList(List<Row> list)
        {
            List<Row> newList = new List<Row>();

            foreach (Row item in list)
            {
                newList.Add(item.Clone());
            }
            return newList;            
        }
    }
}
