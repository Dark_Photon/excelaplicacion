﻿using ExcelAplicacion.Classes;
using ExcelAplicacion.Configuration;

namespace ExcelAplicacion
{
    static class Program
    {
        static void Main(string[] args)
        {
            Config config = Json<Config>.GetJson("Configuration//Config.json");

            string directoryOut = config.DirectoryOut;
            Util util = new Util();

            if (!util.ExistsDirectory(directoryOut))
            {
                util.CreateDirectory(config.DirectoryOut);
            }

            Excel excel = new Excel(config);
            excel.Read();
            excel.ProcessData();       
            excel.CopyProcessInfoForEachTable();
            excel.CreateExcel();
        }

    }
}
