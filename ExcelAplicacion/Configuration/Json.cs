﻿using Newtonsoft.Json;
using System.IO;


namespace ExcelAplicacion.Configuration
{
    public class Json<T>
    {
        protected Json() { }
        public static T GetJson(string ruta)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(ruta));
        }
    }
}
