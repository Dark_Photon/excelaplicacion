﻿using ExcelAplicacion.Classes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ExcelAplicacion.Configuration
{
    public class Config
    {
        [JsonProperty("NameFile")]
        public string NameFile { get; set; }

        [JsonProperty("Directory")]
        public string Directory { get; set; }

        [JsonProperty("CorrectHeaders")]
        public List<Row> CorrectHeaders { get; set; }

        [JsonProperty("IncorrectHeaders")]
        public List<Row> IncorrectHeaders { get; set; }

        [JsonProperty("NameFileOut")]
        public string NameFileOut { get; set; }

        [JsonProperty("DirectoryOut")]
        public string DirectoryOut { get; set; }
    }
}
