﻿using ExcelAplicacion.Classes;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;


namespace ExcelAplicacionTest
{
    [TestFixture]
    class RowTest
    {
        private static Row objectTest1 = new Row(new List<string>() { "1" });
        private static Row objectTest2 = new Row(new List<string>() { "2", "2" });
        private static Row objectTest3 = new Row(new List<string>() { "3", "3", "3" });
        private static Row objectTest4 = new Row(new List<string>() { "4", "4", "4", "4" });
        private static Row objectTest7 = new Row(new List<string>() { "7", "7", "7", "7", "7", "7", "7" });


        private static Row objectEmptyTest1 = new Row(new List<string>() { });
        private static Row objectEmptyTest2 = new Row(new List<string>() { "" });
        private static Row objectEmptyTest3 = new Row(new List<string>() { "", "" });


        private static List<Row> listTest1 = new List<Row>() { objectTest1 };
        private static List<Row> listTest2 = new List<Row>() { objectTest1, objectTest2 };
        private static List<Row> listTest3 = new List<Row>() { objectTest1, objectTest2, objectTest3 };
        private static List<Row> listTest4 = new List<Row>() { objectTest1, objectTest2, objectTest3, objectTest4 };

        private static List<Row> listTestFail1 = new List<Row>() { objectTest4 };
        private static List<Row> listTestFail2 = new List<Row>() { objectTest1, objectTest3 };
        private static List<Row> listTestFail3 = new List<Row>() { objectTest3, objectTest4, objectTest7 };



        public static IEnumerable InputParameterIsEqualTest1
        {
            get
            {
                yield return new TestCaseData(true, objectTest1, objectTest1);
                yield return new TestCaseData(true, objectTest3, objectTest3);
                yield return new TestCaseData(true, objectTest4, objectTest4);

                yield return new TestCaseData(false, objectTest1, objectEmptyTest3);
                yield return new TestCaseData(false, objectTest7, objectTest4);
                yield return new TestCaseData(false, objectTest2, objectTest1);
            }
        }
        [Test]
        [TestCaseSource("InputParameterIsEqualTest1")]
        public void IsEqualTest1(bool expected, Row row, Row otherRow)
        {
            Assert.AreEqual(expected, row.IsEqual(otherRow));
        }


        public static IEnumerable InputParameterIsEqualTest2
        {
            get
            {
                yield return new TestCaseData(true, objectTest1, objectTest1.GetListData());
                yield return new TestCaseData(true, objectTest3, objectTest3.GetListData());
                yield return new TestCaseData(true, objectTest4, objectTest4.GetListData());

                yield return new TestCaseData(false, objectTest1, new List<string>() { "test1" });
                yield return new TestCaseData(false, objectTest2, new List<string>() { "test2", "test2" });
                yield return new TestCaseData(false, objectTest7, new List<string>() { "test3", "test3", "tes3" });
            }
        }
        [Test]
        [TestCaseSource("InputParameterIsEqualTest2")]
        public void IsEqualTest2(bool expected, Row row, List<string> listString)
        {
            Assert.AreEqual(expected, row.IsEqual(listString));
        }


        public static IEnumerable InputParameterIsEmptyTest
        {
            get
            {
                yield return new TestCaseData(true, objectEmptyTest1);
                yield return new TestCaseData(true, objectEmptyTest2);
                yield return new TestCaseData(true, objectEmptyTest3);

                yield return new TestCaseData(false, objectTest1);
                yield return new TestCaseData(false, objectTest2);
                yield return new TestCaseData(false, objectTest3);
            }
        }
        [Test]
        [TestCaseSource("InputParameterIsEmptyTest")]
        public void IsEmptyTest(bool expected, Row row)
        {
            Assert.AreEqual(expected, row.IsEmpty());
        }


        public static IEnumerable InputParameterGetIndexListTest
        {
            get
            {
                yield return new TestCaseData(true, 1, listTest1);
                yield return new TestCaseData(true, 2, listTest2);
                yield return new TestCaseData(true, 3, listTest3);
                yield return new TestCaseData(true, 4, listTest4);

                yield return new TestCaseData(false, 1, listTestFail1);
                yield return new TestCaseData(false, 2, listTestFail2);
                yield return new TestCaseData(false, 49, listTestFail3);
            }
        }
        [Test]
        [TestCaseSource("InputParameterGetIndexListTest")]
        public void GetIndexListTest(bool expected, int maxLength, List<Row> listRows)
        {
            Assert.AreEqual(expected, maxLength.Equals(Row.GetMaxLength(listRows)));
        }


        public static IEnumerable InputParameterGetMaxLengthTest
        {
            get
            {
                yield return new TestCaseData(true, 1, listTest1);
                yield return new TestCaseData(true, 2, listTest2);
                yield return new TestCaseData(true, 3, listTest3);
                yield return new TestCaseData(true, 4, listTest4);

                yield return new TestCaseData(false, 1, listTestFail1);
                yield return new TestCaseData(false, 2, listTestFail2);
                yield return new TestCaseData(false, 49, listTestFail3);
            }
        }

        [Test]
        [TestCaseSource("InputParameterGetMaxLengthTest")]
        public void GetMaxTest(bool expected, int maxLength, List<Row> listRows)
        {
            Assert.AreEqual(expected, maxLength.Equals(Row.GetMaxLength(listRows)));
        }      
    }
}
