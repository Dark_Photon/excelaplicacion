﻿using ExcelAplicacion.Classes;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace ExcelAplicacionTest
{
    [TestFixture]
    class TableTest
    {
        private static Row headerTest1 = new Row(new List<string>() { "ItemTest1", "ItemTest2", "ItemTest3", "ItemTest4" });
        private static Row headerTest2 = new Row(new List<string>() { "ItemTest10", "ItemTest20", "ItemTest30", "ItemTest40" });

        private static Row rowTest1 = new Row(new List<string>() { "item1", "item2", "item3", "item4" });
        private static Row rowTest2 = new Row(new List<string>() { "item1", "item2", "item3", "item4" });
        private static Row rowTest3 = new Row(new List<string>() { "item1", "item2", "item3", "item4" });

        private static Row rowTest4 = new Row(new List<string>() { "otro", "otro", "otro", "otro" });
        private static Row rowTest5 = new Row(new List<string>() { "", "", "", "" });
        private static Row rowTest6 = new Row(new List<string>() { "", "1", "", "" });

        private static Row rowTest7 = new Row(new List<string>() { "otro", "1", "otro", "otro" });

        private static List<Row> listRows = new List<Row>() { rowTest1, rowTest2, rowTest3 };

        private static Table tableWithHeaderTest1 = new Table(headerTest1);
        private static Table tableWithHeaderTest2 = new Table(headerTest2);


        private static Table tableWithHeaderAndListRowsTest1 = new Table(headerTest1, listRows);
        private static Table tableWithHeaderAndListRowsTest2 = new Table(headerTest2, listRows);

        private static Table tableWithHeaderAndListRowsTest3 = new Table(headerTest1, listRows);
        private static Table tableWithHeaderAndListRowsTest4 = new Table(headerTest2, listRows);


        private static List<Row> listRowsWithSomeyEmptyCells1 = new List<Row>() { rowTest1, rowTest2, rowTest3, rowTest5, rowTest5 };
        private static Table tableCopyProcessTest1 = new Table(headerTest1, listRowsWithSomeyEmptyCells1);

        private static List<Row> listRowsFinalTest1 = new List<Row>() { rowTest1, rowTest2, rowTest3, rowTest1, rowTest2, };
        private static Table tableCopyProcessFinalTest1 = new Table(headerTest1, listRowsFinalTest1);


        private static List<Row> listRowsWithSomeyEmptyCells2 = new List<Row>() { rowTest1, rowTest2, rowTest3, rowTest4, rowTest5, rowTest6, };
        private static Table tableCopyProcessTest2 = new Table(headerTest1, listRowsWithSomeyEmptyCells2);

        private static List<Row> listRowsFinalTest2 = new List<Row>() { rowTest1, rowTest2, rowTest3, rowTest4, rowTest4, rowTest7, };
        private static Table tableCopyProcessFinalTest2 = new Table(headerTest1, listRowsFinalTest2);

        public static IEnumerable InputParameterIsEqualHeaderTest1
        {
            get
            {
                yield return new TestCaseData(true, tableWithHeaderTest1, headerTest1);
                yield return new TestCaseData(true, tableWithHeaderTest2, headerTest2);
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest1, headerTest1);
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest2, headerTest2);

                yield return new TestCaseData(false, tableWithHeaderTest1, headerTest2);
                yield return new TestCaseData(false, tableWithHeaderAndListRowsTest1, headerTest2);
            }
        }

        [Test]
        [TestCaseSource("InputParameterIsEqualHeaderTest1")]
        public void IsEqualHeaderTest1(bool expected, Table table, Row row)
        {
            Assert.AreEqual(expected, table.IsEqualHeader(row));
        }

        public static IEnumerable InputParameterIsEqualHeaderTest2
        {
            get
            {
                yield return new TestCaseData(true, tableWithHeaderTest1, headerTest1.GetListData());
                yield return new TestCaseData(true, tableWithHeaderTest2, new List<string>() { "ItemTest10", "ItemTest20", "ItemTest30", "ItemTest40" });
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest1, headerTest1.GetListData());
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest2, new List<string>() { "ItemTest10", "ItemTest20", "ItemTest30", "ItemTest40" });

                yield return new TestCaseData(false, tableWithHeaderTest1, new List<string>() { "ItemTest3", "ItemTest3", "ItemTest3", "ItemTest3" });
                yield return new TestCaseData(false, tableWithHeaderAndListRowsTest1, new List<string>() { "ItemTest4", "ItemTest4", "ItemTest4", "ItemTest4" });
            }
        }

        [Test]
        [TestCaseSource("InputParameterIsEqualHeaderTest2")]
        public void IsEqualHeaderTest2(bool expected, Table table, List<string> list)
        {
            Assert.AreEqual(expected, table.IsEqualHeader(list));
        }

        public static IEnumerable InputParameterIsEqualTest
        {
            get
            {
                yield return new TestCaseData(true, tableWithHeaderTest1, tableWithHeaderTest1);
                yield return new TestCaseData(true, tableWithHeaderTest2, tableWithHeaderTest2);
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest1, tableWithHeaderAndListRowsTest3);
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest2, tableWithHeaderAndListRowsTest4);

                yield return new TestCaseData(false, tableWithHeaderTest1, tableWithHeaderTest2);
                yield return new TestCaseData(false, tableWithHeaderAndListRowsTest1, tableWithHeaderAndListRowsTest2);
            }
        }

        [Test]
        [TestCaseSource("InputParameterIsEqualTest")]
        public void IsEqualTest(bool expected, Table table, Table otherTable)
        {
            Assert.AreEqual(expected, table.IsEqual(otherTable));
        }

        public static IEnumerable InputParameterCopyProcessInfoTest
        {
            get
            {
                yield return new TestCaseData(true, tableCopyProcessTest1, tableCopyProcessFinalTest1);
                yield return new TestCaseData(true, tableCopyProcessTest2, tableCopyProcessFinalTest2);
            }
        }

        [Test]
        [TestCaseSource("InputParameterCopyProcessInfoTest")]
        public void CopyProcessInfoTest(bool expected, Table table, Table tableFinal)
        {
            table.CopyProcessInfo();
            Assert.AreEqual(expected, table.IsEqual(tableFinal));
        }

        public static IEnumerable InputParameterConvertToDataTableTest
        {
            get
            {
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest1, 4, 3);
                yield return new TestCaseData(true, tableWithHeaderAndListRowsTest2, 4, 3);
                yield return new TestCaseData(true, tableWithHeaderTest1, 4, 0);
            }
        }

        [Test]
        [TestCaseSource("InputParameterConvertToDataTableTest")]
        public void ConvertToDataTableTest(bool expected, Table table, int columns, int rows)
        {
            DataTable dt = table.ConvertTableToDataTable();

            Assert.AreEqual(expected, dt.Columns.Count == columns && dt.Rows.Count == rows);
        }


    }
}
