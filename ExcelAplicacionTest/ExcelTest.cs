﻿using ExcelAplicacion.Classes;
using ExcelAplicacion.Configuration;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ExcelAplicacionTest
{
    [TestFixture]
    class ExcelTest
    {

        private static Row headerTest = new Row(new List<string>() { "TestItem1", "TestItem2", "TestItem3", "TestItem4", "TestItem5", "TestItem6", "TestItem7" });

        private static Row rowTest1 = new Row(new List<string>() { "1", "1", "1", "1", "1", "1", "1" });
        private static Row rowTest2 = new Row(new List<string>() { "1", "2", "3", "4", "5", "6", "7" });
        private static Row rowTest3 = new Row(new List<string>() { "1", "", "", "", "", "", "" });
        private static Row rowTest4 = new Row(new List<string>() { "", "2", "", "", "", "", "" });
        private static Row rowTest5 = new Row(new List<string>() { "", "", "3", "", "", "", "" });


        private static List<Row> listRowsTest = new List<Row>()
        { 
            rowTest1,
            rowTest2,
            rowTest3,
            rowTest4,
            rowTest5,
            rowTest1,

            rowTest1,
            rowTest2,
            rowTest3,
            rowTest4,
            rowTest5,
            rowTest2
        };

        private static Table tableTest = new Table(headerTest,listRowsTest);

        
        public static IEnumerable InputParameterFilterTest
        {
            get
            {
                yield return new TestCaseData(true, tableTest);              
            }
        }
        
        [Test]
        [TestCaseSource("InputParameterFilterTest")]
        public void FilterTest(bool expected, Table TableTest)
        {
            Config config = Json<Config>.GetJson("ConfigurationTest//ConfigTest.json");

            string directoryOut = config.DirectoryOut;
            Util util = new Util();

            if (!util.ExistsDirectory(directoryOut))
            {
                util.CreateDirectory(config.DirectoryOut);
            }

            Excel excel = new Excel(config);
            excel.Read();
            List<Table> listTablesTest = excel.filter(excel.ListRows, config.CorrectHeaders, config.IncorrectHeaders);

            Assert.AreEqual(expected, tableTest.IsEqual(listTablesTest.First()));            
        }

    }
}
